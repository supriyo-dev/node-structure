
exports.up = function(knex, Promise) {

	return knex.schema.createTable('payoutnote', function (table) {
        table.increments();
        table.string('title')
        table.text('description')
        table.integer('payout_id');
    })
  	
  	table.foreign('payout_id').references('emppayout.id').onDelete('CASCADE');
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('emppayout');
};
