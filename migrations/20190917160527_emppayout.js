
exports.up = function(knex, Promise) {

	return knex.schema.createTable('emppayout', function (table) {
        table.increments();
        table.float('gross_pay');
        table.float('ta');
        table.float('da');
        table.float('total_pay');
        table.integer('emp_id');
    })
  
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('emppayout');
};