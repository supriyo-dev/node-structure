/*
|--------------------------------------------------------------------------
| Application Helper Router Import
|--------------------------------------------------------------------------
|
*/
const Router = Helper('router');
var jwt = require('jsonwebtoken');
//=========================================================================
/*
|--------------------------------------------------------------------------
| Application All API Routes
|--------------------------------------------------------------------------
|
*/
//==Define a api group
Router._group('/api', function (Router) {
    //=====================================================================
    //==sample test urls
    Router.get('/test', (req, res, next) => {
        res.status(200).json(res.fnSuccess('Node JS V1.0 API Running ........'));
    }); 

    /*	To set route indivisual 
	    Router._get('/emplist', 'EmployeesController.index');
	    Router._post('/empadd', 'EmployeesController.store');
	    Router._get('/find/:id', 'EmployeesController.show');
	    Router._post('/update/:id', 'EmployeesController.update');
	    Router._post('/destroy/:id', 'EmployeesController.destroy');    
	*/
    //to set all route as recourse
    Router._group('/emp', function (Router) {
    	Router._resource_api('/emplist', 'EmployeesController');
    	Router._get('/sendmail', 'EmployeesController.sendMail');
    });

    Router._group('/emppayout', function (Router) {
    	Router._resource_api('/payouts', 'PayoutsController');
    	Router._get('/showUpdateList/:id', 'PayoutsController.showUpdateList');
    });

    Router._group('/payoutnote', function (Router) {
    	Router._resource_api('/nots', 'NoteController');
    	Router._get('/showAllPayoutNote/:id', 'NoteController.showAllPayoutNote');
    });

    Router._group('/imageupload', function (Router) {
    	Router._resource_api('/images', 'EmpimagesController');
    	
    });    

});
//=========================================================================
module.exports = Router;