const Mail = Helper('mail');

module.exports = function(details){    
    const mailOptions = {
        to:details.to, 
        subject:details.subject,
        template:{
            path : 'emailTemplateTest',
            data : {contact:details}
        }
    };
    return Mail(mailOptions);
}