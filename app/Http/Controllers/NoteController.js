/****************************************************
# NoteController            
# Page/Class name : NoteController
# Author          : NA
# Created Date    : 19/09/2019
# Functionality   : index, store, show, update, destroy
# Purpose         : For All crud operation
*****************************************************/
//==================================================================
//==Include requre modules and files.

const Validator = Helper('validator');
const md5       = require('md5');
const slugify   = require('slug-generator');
const base64Img = require('base64-img');

//db models
const ModelNote  = Models('payoutnote');

//==================================================================
const NoteController = {
    /****************************************************      
    # Function name : index
    # Author        : NA
    # Created Date  : 19/09/2019               
    # Purpose       : For showing all records. 
    # Params        : [req, res, next]  
    *****************************************************/
    index: async function (req, res, next) {

        // let payout_id = _.toInteger(req.query.payout_id) ? req.query.payout_id : false;
        // let is_payout = req.query.is_payout ? req.query.is_payout : false;

         let payout_id = _.toInteger(req.query.payout_id) ? req.query.payout_id : false;
         let is_payout = req.query.is_payout ? req.query.is_payout : false;

         //console.log(payout_id, is_payout);

        let modelNote = ModelNote.forge().orderBy('id');

        if(payout_id && is_payout === 'true'){
            modelNote = modelNote.where('payout_id', payout_id);
        }
        
		modelNote.fetchAll().then((data) => {
		    let responses = data.toJSON();
		    console.log(responses);
            return res.status(200).json(res.fnSuccess(responses));

		}).catch((errors) => {
		    return res.status(400).json(res.fnError(errors));
		});      

    },
    //=====================================================
    /****************************************************      
    # Function name : store
    # Author        : NA
    # Created Date  : 19/09/2019                   
    # Purpose       : For store a particular payout records.
    # Params        : [req, res, next] 
    *****************************************************/
    store: async function (req, res, next) {

        // validation for data input        
        let formData = req.body;
        let validation = new Validator(formData, {
            title       : 'required|string',
            description : 'required|string',
            date        : 'required|dateFormat:YYYY-MM-DD'
        });
        let matched = await validation.check();
        if (!matched) {
            return res.status(200).json(res.fnError(validation.errors));
        }            
        // configure data for insert
        const insert = {
            title       : req.body.title,
            description : req.body.description,
            date        : req.body.date,
            payout_id   : req.body.payout_id
        }

        new ModelNote(insert).save().then((data) => {
            let responses = data.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(responses));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });

    },
    //=====================================================
    /****************************************************      
    # Function name : show
    # Author        : NA 
    # Created Date  : 19/09/2019                 
    # Purpose       : To show a particular records by Ids
    # Params        : [req, res, next]  
    *****************************************************/
    show: function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;        
        
        modelNote.where('id', id).fetchAll().then((Response) => {
            let responses = Response.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================
    /****************************************************      
    # Function name : update
    # Author        : NA
    # Created Date  : 19/09/2019                 
    # Purpose       : To update a paritcular records by its Ids
    # Params        : [req, res, next]  
    *****************************************************/
    update: async function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;
        console.log(id);
        //validation for the update
        let formData = req.body;
        let validation = new Validator(formData, {
             title       : 'required|string',
             description : 'required|string',
             date        : 'required|dateFormat:YYYY-MM-DD'                    
        });

        let matched = await validation.check();

        if (!matched) {
            return res.status(200).json(res.fnError(validation.errors));
        }

        //configure data for the update       
        const updateData = {
            title       : req.body.title,
            description : req.body.description,
            date        : req.body.date,
        }

        ModelNote.where('id', id).save(updateData, { patch: true }).then((Response) => {
            let responses = Response.toJSON();
            //console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================
    /****************************************************      
    # Function name : destroy
    # Author        : NA
    # Created Date  : 19/09/2017                
    # Purpose       : For delete a particular record by its Ids
    # Params        : [req, res, next]  
    *****************************************************/
    destroy: function (req, res, next) {
        
        //let id = req.body.id;
        let id = _.toInteger(req.params.id) ? req.params.id : false;

        ModelNote.where('id', id).destroy({ require: false }).then((response) => {
            return res.status(200).json(res.fnSuccess(response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================

    //=====================================================
    /****************************************************      
    # Function name : showAllPayoutNote
    # Author        : NA
    # Created Date  : 19/09/2017                
    # Purpose       : For show all note of a particular payout 
    # Params        : [req, res, next]  
    *****************************************************/
    showAllPayoutNote: function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;
        
        ModelNote.where('payout_id', id).fetchAll().then((Response) => {
            let responses = Response.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================
    
}
//==================================================================
module.exports = NoteController;
//==================================================================