/****************************************************
# EmployeesController            
# Page/Class name : EmployeesController
# Author          : NA
# Created Date    : 19/09/2019
# Functionality   : index, store, show, update, destroy
# Purpose         : For All crud operation
*****************************************************/
//==================================================================
//==Include requre modules and files.

const Validator = Helper('validator');
const md5       = require('md5');
const slugify   = require('slug-generator');
const base64Img = require('base64-img');


const EmailTest = Mails('EmailTest');
const ModelEmp  = Models('employees');

//==================================================================
const EmployeesController = {
    /****************************************************      
    # Function name : index
    # Author        : NA
    # Created Date  : 19/09/2019               
    # Purpose       : For showing all records and relationship data maped with `emppayout`,`payoutnote` tables.
    # Params        : [req, res, next]  
    *****************************************************/
    index: async function (req, res, next) {
        
		ModelEmp.fetchAll({withRelated: ['empPayout', 'empPayout.payoutNote']}).then((data) => {
		    let responses = data.toJSON();
		    console.log(responses);
            //console.log(responses.empPayout);
            return res.status(200).json(res.fnSuccess(responses));

		}).catch((errors) => {
		    return res.status(400).json(res.fnError(errors));
		});
    },
    //=====================================================
    /****************************************************      
    # Function name : store
    # Author        : NA
    # Created Date  : 19/09/2019                   
    # Purpose       : For store a particular emp records.
    # Params        : [req, res, next] 
    *****************************************************/
    store: async function (req, res, next) {

        // validation for data input        
        let formData = req.body;
        let validation = new Validator(formData, {
            name         : 'required|string',
            email        : 'required|string',
            designation  : 'required|string',
            phoneNumber  : 'required|minLength:10|maxLength:15',
                    
        });
        let matched = await validation.check();
        if (!matched) {
            return res.status(200).json(res.fnError(validation.errors));
        }            
        // configure data for insert
        const insert = {
            name        : req.body.name,
            email       : req.body.email,
            designation : req.body.designation,
            phoneNumber : req.body.phoneNumber
        }

        new ModelEmp(insert).save().then((data) => {
            let responses = data.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(responses));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });

    },
    //=====================================================
    /****************************************************      
    # Function name : show
    # Author        : NA 
    # Created Date  : 19/09/2019                 
    # Purpose       : To show a particular records by Ids
    # Params        : [req, res, next]  
    *****************************************************/
    show: function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;
        
        ModelEmp.where('id', id).fetchAll().then((Response) => {
            let responses = Response.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================
    /****************************************************      
    # Function name : update
    # Author        : NA
    # Created Date  : 19/09/2019                 
    # Purpose       : To update a paritcular records by its Ids
    # Params        : [req, res, next]  
    *****************************************************/
    update: async function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;
        
        //validation for the update
        let formData = req.body;
        let validation = new Validator(formData, {
            name           : 'required|string',
            email          : 'required|string',
            designation    : 'required|string',
            phoneNumber    : 'required|minLength:10|maxLength:15',
                    
        });

        let matched = await validation.check();

        if (!matched) {
            return res.status(200).json(res.fnError(validation.errors));
        }

        //configure data for the update       
        const updateData = {
            name        : formData.name,
            email       : formData.email,
            designation : formData.designation,
            phoneNumber : formData.phoneNumber,
        }

        ModelEmp.where('id', id).save(updateData, { patch: true }).then((Response) => {
            let responses = Response.toJSON();
            //console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================
    /****************************************************      
    # Function name : destroy
    # Author        : NA
    # Created Date  : 19/09/2017                
    # Purpose       : For delete a particular record by its Ids
    # Params        : [req, res, next]  
    *****************************************************/
    destroy: function (req, res, next) {
        
        //let id = req.body.id;
        let id = _.toInteger(req.params.id) ? req.params.id : false;

        ModelEmp.where('id', id).destroy({ require: false }).then((response) => {
            return res.status(200).json(res.fnSuccess(response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================

    //=====================================================
    /****************************************************      
    # Function name : sendMail
    # Author        : NA
    # Created Date  : 19/09/2019                
    # Purpose       : For send a particular mail with records
    # Params        : [req, res, next]  
    *****************************************************/
    sendMail: function (req, res, next) {
        
        let data = {
            to      : 'ron@yopmail.com',
            subject : 'My subjects',
            title   : 'My title',
            message : 'my content',
        }
        EmailTest(data).then(function(responce){
            console.log(responce);
            res.end();
        });
    }
    //=====================================================

}
//==================================================================
module.exports = EmployeesController;
//==================================================================