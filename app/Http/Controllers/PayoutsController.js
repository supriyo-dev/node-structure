/****************************************************
# PayoutsController            
# Page/Class name : PayoutsController
# Author          : NA
# Created Date    : 19/09/2019
# Functionality   : index, store, show, update, destroy
# Purpose         : For All crud operation
*****************************************************/
//==================================================================
//==Include requre modules and files.

const Validator = Helper('validator');
const md5       = require('md5');
const slugify   = require('slug-generator');
const base64Img = require('base64-img');

//db models
const ModelPayout  = Models('emppayout');

//==================================================================
const PayoutsController = {
    /****************************************************      
    # Function name : index
    # Author        : NA
    # Created Date  : 19/09/2019               
    # Purpose       : For showing all records and relationship data maped with `payoutnote` tables.
    # Params        : [req, res, next]  
    *****************************************************/
    index: async function (req, res, next) {
        
		ModelPayout.fetchAll({withRelated: ['payoutNote']}).then((data) => {
		    let responses = data.toJSON();
		    console.log(responses);
            return res.status(200).json(res.fnSuccess(responses));

		}).catch((errors) => {
		    return res.status(400).json(res.fnError(errors));
		});
    },
    //=====================================================
    /****************************************************      
    # Function name : store
    # Author        : NA
    # Created Date  : 19/09/2019                   
    # Purpose       : For store a particular payout records.
    # Params        : [req, res, next] 
    *****************************************************/
    store: async function (req, res, next) {

        // validation for data input        
        let formData = req.body;
        let validation = new Validator(formData, {
            grossPayment   : 'required|numeric',
            inputDa        : 'required|numeric',
            inputTa        : 'required|numeric',
            totalAmount    : 'required|numeric',

        });
        let matched = await validation.check();
        if (!matched) {
            return res.status(200).json(res.fnError(validation.errors));
        }            
        // configure data for insert
        const insert = {
            grossPayment  : req.body.grossPayment,
            inputDa       : req.body.inputDa,
            inputTa       : req.body.inputTa,
            totalAmount   : req.body.totalAmount,
            emp_id        : req.body.empId
        }

        new ModelPayout(insert).save().then((data) => {
            let responses = data.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(responses));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });

    },
    //=====================================================
    /****************************************************      
    # Function name : show
    # Author        : NA 
    # Created Date  : 19/09/2019                 
    # Purpose       : To show a particular records by Ids
    # Params        : [req, res, next]  
    *****************************************************/
    show: function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;
        
        ModelPayout.where('id', id).fetchAll().then((Response) => {
            let responses = Response.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================
    /****************************************************      
    # Function name : update
    # Author        : NA
    # Created Date  : 19/09/2019                 
    # Purpose       : To update a paritcular records by its Ids
    # Params        : [req, res, next]  
    *****************************************************/
    update: async function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;
        console.log(id);
        //validation for the update
        let formData = req.body;
        let validation = new Validator(formData, {
            grossPayment   : 'required|numeric',
            inputDa        : 'required|numeric',
            inputTa        : 'required|numeric',
            totalAmount    : 'required|numeric',
                    
        });

        let matched = await validation.check();

        if (!matched) {
            return res.status(200).json(res.fnError(validation.errors));
        }

        //configure data for the update       
        const updateData = {
            grossPayment  : formData.grossPayment,
            inputDa       : formData.inputDa,
            inputTa       : formData.inputTa,
            totalAmount   : formData.totalAmount,
        }

        ModelPayout.where('id', id).save(updateData, { patch: true }).then((Response) => {
            let responses = Response.toJSON();
            //console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================
    /****************************************************      
    # Function name : destroy
    # Author        : NA
    # Created Date  : 19/09/2017                
    # Purpose       : For delete a particular record by its Ids
    # Params        : [req, res, next]  
    *****************************************************/
    destroy: function (req, res, next) {
        
        //let id = req.body.id;
        let id = _.toInteger(req.params.id) ? req.params.id : false;

        ModelPayout.where('id', id).destroy({ require: false }).then((response) => {
            return res.status(200).json(res.fnSuccess(response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================

    //=====================================================
    /****************************************************      
    # Function name : showUpdateList
    # Author        : NA
    # Created Date  : 19/09/2017                
    # Purpose       : For show the selected list of a particular employee
    # Params        : [req, res, next]  
    *****************************************************/
    showUpdateList: function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;
        
        ModelPayout.where('emp_id', id).fetchAll().then((Response) => {
            let responses = Response.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================

}
//==================================================================
module.exports = PayoutsController;
//==================================================================