/****************************************************
# EmpimagesController            
# Page/Class name : EmpimagesController
# Author          : NA
# Created Date    : 19/09/2019
# Functionality   : index, store, show, update, destroy
# Purpose         : For All crud operation
*****************************************************/
//==================================================================
//==Include requre modules and files.

const Validator = Helper('validator');
const md5       = require('md5');
const slugify   = require('slug-generator');
const base64Img = require('base64-img');

//db models
const ModelEmpImages  = Models('empimages');

//==================================================================
const EmpimagesController = {
    /****************************************************      
    # Function name : index
    # Author        : NA
    # Created Date  : 19/09/2019               
    # Purpose       : For showing all records and relationship data maped with `payoutnote` tables.
    # Params        : [req, res, next]  
    *****************************************************/
    index: async function (req, res, next) {
        
        let emp_id     = _.toInteger(req.query.emp_id) ? req.query.emp_id : false;
        let is_filter  = req.query.is_filter ? req.query.is_filter : false;

        console.log(emp_id, is_filter);

        let modelEmpImages = ModelEmpImages.forge().orderBy('id');

        if(emp_id && is_filter === 'true'){
            modelEmpImages = modelEmpImages.where('emp_id', emp_id);
        }

		modelEmpImages.fetchAll().then((data) => {
		    let responses = data.toJSON();
		    console.log(responses);
            return res.status(200).json(res.fnSuccess(responses));

		}).catch((errors) => {
		    return res.status(400).json(res.fnError(errors));
		});
    },
    //=====================================================
    /****************************************************      
    # Function name : store
    # Author        : NA
    # Created Date  : 19/09/2019                   
    # Purpose       : For store a particular payout records.
    # Params        : [req, res, next] 
    *****************************************************/
    store: async function (req, res, next) {

        //console.log(req.body.image_file);
        let image_source = req.body.image_file.replace("data:image/*;charset=utf-8;base64,", "data:image/jpg;base64,"); 

        let base64img       = image_source.split(' ').join('+'); // Replace empty space 
        let destinationPath = publicPath + '/uploads/'; // File destination path define 
        let now             = new Date();
        let slug            = 'profile';     
        let new_filename    = now.getTime() + '_' + slug; // File Name Creating 

        base64Img.img(base64img, destinationPath, new_filename, function (err, filepath) { 
            console.log(err); console.log(filepath); 
        });
                    
        //configure data for insert
        const insert = {
            image_name           : new_filename + '.png',
            image_description    : req.body.image_description,
            emp_id               : req.body.emp_id
        }

        new ModelEmpImages(insert).save().then((data) => {
            let responses = data.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(responses));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });

    },
    //=====================================================
    /****************************************************      
    # Function name : show
    # Author        : NA 
    # Created Date  : 19/09/2019                 
    # Purpose       : To show a particular records by Ids
    # Params        : [req, res, next]  
    *****************************************************/
    show: function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;
        
        ModelEmpImages.where('id', id).fetchAll().then((Response) => {
            let responses = Response.toJSON();
            console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================
    /****************************************************      
    # Function name : update
    # Author        : NA
    # Created Date  : 19/09/2019                 
    # Purpose       : To update a paritcular records by its Ids
    # Params        : [req, res, next]  
    *****************************************************/
    update: async function (req, res, next) {
        
        let id = _.toInteger(req.params.id) ? req.params.id : false;
        console.log(id);
        //validation for the update
        let formData = req.body;
        let validation = new Validator(formData, {
            grossPayment   : 'required|numeric',
            inputDa        : 'required|numeric',
            inputTa        : 'required|numeric',
            totalAmount    : 'required|numeric',
                    
        });

        let matched = await validation.check();

        if (!matched) {
            return res.status(200).json(res.fnError(validation.errors));
        }

        //configure data for the update       
        const updateData = {
            grossPayment  : formData.grossPayment,
            inputDa       : formData.inputDa,
            inputTa       : formData.inputTa,
            totalAmount   : formData.totalAmount,
        }

        ModelEmpImages.where('id', id).save(updateData, { patch: true }).then((Response) => {
            let responses = Response.toJSON();
            //console.log(responses);
            return res.status(200).json(res.fnSuccess(Response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================
    /****************************************************      
    # Function name : destroy
    # Author        : NA
    # Created Date  : 19/09/2017                
    # Purpose       : For delete a particular record by its Ids
    # Params        : [req, res, next]  
    *****************************************************/
    destroy: function (req, res, next) {
        
        //let id = req.body.id;
        let id = _.toInteger(req.params.id) ? req.params.id : false;

        ModelEmpImages.where('id', id).destroy({ require: false }).then((response) => {
            return res.status(200).json(res.fnSuccess(response));
        }).catch((errors) => {
            return res.status(400).json(res.fnError(errors));
        });
    },
    //=====================================================

}
//==================================================================
module.exports = EmpimagesController;
//==================================================================