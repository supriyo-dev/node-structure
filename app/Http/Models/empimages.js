/****************************************************
# empimagesModel            
# Page/Class name : empimagesModel
# Author          : NA
# Created Date    : 19/09/2019
# Functionality   : Model for `emppayout` table
# Purpose         : To process all the data of the `payoutnote` table 
*****************************************************/

const result = require('dotenv').config();
const applicationApp = Config('app');

let empimagesModel = dbConn.Model.extend({
    tableName: 'empimages',
    //============================================
    /****************************************************      
    # Function name : NA
    # Author        : NA
    # Created Date  : NA                 
    # Purpose       : Na
    # Params        : [NA]
    *****************************************************/
    //============================================
});

module.exports = empimagesModel;