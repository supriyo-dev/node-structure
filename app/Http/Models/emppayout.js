/****************************************************
# emppayoutModel            
# Page/Class name : emppayoutModel
# Author          : 
# Created Date    : 18/09/2019
# Functionality   : Model for `emppayout` table related hasMany with `payoutnote` table
# Purpose         : To process all the data of the `emppayout` table  
*****************************************************/

const result = require('dotenv').config();
const applicationApp = Config('app');

let emppayoutModel = dbConn.Model.extend({
    tableName: 'emppayout',
    //============================================
    /****************************************************      
    # Function name   : payoutNote
    # Author          : NA
    # Created Date    : 19/09/2017                 
    # Purpose         : map all the rows related with `id`  
    # Params          : []
    *****************************************************/
    payoutNote: function () {
        return this.hasMany(Models('payoutnote'), 'payout_id');
    }, 
    //============================================

});

module.exports = emppayoutModel;