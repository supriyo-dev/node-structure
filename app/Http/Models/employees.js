/****************************************************
# employeesModel            
# Page/Class name : employeesModel
# Author          : 
# Created Date    : 18/09/2019
# Functionality   : Model for `employees` table related hasMany with `emppayout` table
# Purpose         : To process all the data of the `employees` table 
*****************************************************/

const result = require('dotenv').config();
const applicationApp = Config('app');

let employeesModel = dbConn.Model.extend({
    tableName: 'employees',
    //============================================
    /****************************************************      
    # Function name  : empPayout
    # Author         : 
    # Created Date   : 19//09/2019                  
    # Purpose        : map all the rows related with `id`  
    # Params         : [NA]
    *****************************************************/
    empPayout: function () {
        return this.hasMany(Models('emppayout'), 'emp_id');
    }, 
    //============================================

});

module.exports = employeesModel;